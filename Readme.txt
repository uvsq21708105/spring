La branche master du dépot contient toujours une version stable.
Pour chaque question, on a créé une ou plusieurs branches de test. 
Ces branches de test sont fusionnées avec la branche master une fois le résultat est stable.

Chaque question correspond à un ou plusieurs commits taggés (v1.X pour la question 1, v2.X pour la question 2, etc.)

Le dernier commit, taggé en v5.1, est une version stable qui permet de tester toutes les questions de l'exercice :


* Question 2 :	(Spring Boot : facilite le developpement de l'application)
	
	- Compilation du projet : 						./mvnw package
	- Lancement de l'application : 					java -jar target/spring-boot-demo-0.0.1-SNAPSHOT.jar
		
		# Depuis un autre terminal :
	
		- Requête HTTP GET : 						curl http://localhost:8080/GET
		
		On a rajouté /GET pour différencier le résultat de la question 2 de celui de la question 4


* Question 3 :	(Docker : permet d'isoler l'application et son environnement)
	
	Docker nécéssite un Ubuntu 64-bit
	
	- Compilation du projet et génération de l'image docker :		
													sudo ./mvnw package docker:build
	- Lancement de l'application avec docker :					
													sudo docker run -p 8080:8080 -t spring-boot-demo
	
	Pour résoudre l'erreur "port has already been allocated" : 		sudo service docker restart


* Question 4 et 5 : 

	- Compilation du projet : 						./mvnw package
	- Lancement de l'application : 					java -jar target/spring-boot-demo-0.0.1-SNAPSHOT.jar
		
		# Depuis un autre terminal :
		
	- Requête HTTP GET (pour récupérer les tâches) :			curl http://localhost:8080	
	- Requête HTTP POST (pour ajouter une tâche) :				curl --data 'title=Titre' http://localhost:8080
																	
	La réponse est au format JSON.
