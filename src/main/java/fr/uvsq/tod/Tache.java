package fr.uvsq.tod;

public class Tache {

    private final long numero;
    private final String titre;

    public Tache(long numero, String titre) {
        this.numero = numero;
        this.titre = titre;
    }

    public long getNumero() {
        return numero;
    }

    public String getTitre() {
        return titre;
    }
    
    
    
    
    
}
