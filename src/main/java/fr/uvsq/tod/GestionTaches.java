package fr.uvsq.tod;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.ArrayList;

@RestController
public class GestionTaches {

    private static final String reponse = "Titre de la tache : %s.";
    private final AtomicLong compteurTaches = new AtomicLong();
    
    private List<Tache> ListeTaches = new ArrayList<Tache>();
	
	
	
	//Q4
	@RequestMapping(method = RequestMethod.GET)
    public List<Tache> afficheListeTaches()
    {
		System.out.println("Affichage de la liste des taches\n");
		return ListeTaches;
	}
	
	
	
	
	
	
	//Q5 : ajout d'une tache
    @RequestMapping(method = RequestMethod.POST)
    public void nouvelleTache(@RequestParam(value="title", defaultValue="sans titre") String title) 
    {
        ListeTaches.add( new Tache(compteurTaches.incrementAndGet(), title) );
        
        System.out.println("Tache ajoutée\n");
    }
    

}
